from __future__ import unicode_literals

from django.db import models
from django.utils import timezone

now = timezone.now()

class SlideshowImages(models.Model):
    date_added = models.DateField(default=now)
    title = models.CharField(max_length=150)
    image = models.FileField(upload_to="slideshows", max_length=150)
    text = models.TextField()

    def __str__(self):
        return self.title

class Querys(models.Model):
    date_added = models.DateField(default=now)
    name = models.CharField(max_length=150)
    email = models.EmailField(max_length=150)
    phone = models.CharField(max_length=200)
    message = models.TextField()

    def __str__(self):
        return self.name
